//
// Created by ikmokhirio on 20.11.2019.
//

#include "MainMenu.h"

sf::RectangleShape
MainMenu::generateShape(sf::Vector2f size, sf::Color mainColor, sf::Color outlineColor, float outlineThickness,
                        sf::Vector2f position) {
    sf::RectangleShape shape;

    shape.setSize(size);
    shape.setFillColor(mainColor);
    shape.setOutlineThickness(outlineThickness);
    shape.setOutlineColor(outlineColor);
    shape.setOrigin(shape.getSize().x / 2, shape.getSize().y / 2);
    shape.setPosition(position);

    return shape;
}

sf::Text
MainMenu::generateText(sf::Vector2f position, string textString, const sf::Font &font, sf::Color color,
                       int characterSize) {
    sf::Text text;

    text.setPosition(position);
    text.setString(textString);
    text.setFont(font);
    text.setFillColor(color);
    text.setCharacterSize(characterSize);

    return text;
}

void MainMenu::generateMenu() {

    sf::Vector2f buttonsPosition(cam_->getCenter() - sf::Vector2f(0, 64));
    sf::Vector2f buttonsSize(128, 64);


    infoText_ = generateText((buttonsPosition - sf::Vector2f(224, 32)), INFO_TEXT,
                             DataResources::mainFont, sf::Color::Black, BIG_TEXT);

    inputWidth_ = generateShape(buttonsSize, sf::Color::White, sf::Color::Black, 2.5f, buttonsPosition);
    widthTxt_ = generateText((inputWidth_.getPosition() - inputWidth_.getOrigin()), ZERO_TEXT, DataResources::mainFont,
                             sf::Color::Black, THE_BIGGEST_TEXT);

    inputHeight_ = generateShape(buttonsSize, sf::Color::White, sf::Color::Black, 2.5f,
                                 buttonsPosition + sf::Vector2f(0, 64));
    heightTxt_ = generateText((inputHeight_.getPosition() - inputHeight_.getOrigin()), ZERO_TEXT,
                              DataResources::mainFont,
                              sf::Color::Black, THE_BIGGEST_TEXT);

    inputNum_ = generateShape(buttonsSize, sf::Color::White, sf::Color::Black, 2.5f,
                              buttonsPosition + sf::Vector2f(0, 128));
    numTxt_ = generateText((inputNum_.getPosition() - inputNum_.getOrigin()), ZERO_TEXT, DataResources::mainFont,
                           sf::Color::Black, THE_BIGGEST_TEXT);

    startGameButton_ = generateShape(sf::Vector2f(256, 64), sf::Color::White, sf::Color::Black, 2.5f,
                                     buttonsPosition + sf::Vector2f(0, 224));
    startGameTxt_ = generateText((startGameButton_.getPosition() -
                                  startGameButton_.getOrigin()), START_GAME_TEXT, DataResources::mainFont,
                                 sf::Color::Black, SMALL_TEXT);

    testModeButton_ = generateShape(sf::Vector2f(256, 64), sf::Color::White, sf::Color::Black, 2.5f,
                                    buttonsPosition + sf::Vector2f(0, 320));
    testModeTxt_ = generateText((testModeButton_.getPosition() - testModeButton_.getOrigin()), TEST_MODE_DISABLED,
                                DataResources::mainFont, sf::Color::Black, SMALL_TEXT);

    additionalInfo_ = generateText(sf::Vector2f(cam_->getCenter().x - cam_->getSize().x / 4,
                                                cam_->getCenter().y - cam_->getSize().y / 2), ADDITIONAL_INFO,
                                   DataResources::mainFont, sf::Color::Black, SMALL_TEXT);

    firstMoveButton_ = generateShape(sf::Vector2f(72, 32), sf::Color::White, sf::Color::Black, 2.5f,
                                     buttonsPosition + sf::Vector2f(128, 32));
    firstMoveText_ = generateText((firstMoveButton_.getPosition() - firstMoveButton_.getOrigin()), CROSS_MOVE_TEXT,
                                  DataResources::mainFont, sf::Color::Black, SMALL_TEXT);

}

MainMenu::MainMenu(sf::RenderWindow *window) {
    testMode_ = false;

    gm_ = nullptr;
    window_ = window;
    cam_ = new sf::View(sf::Vector2f(0, 0), sf::Vector2f(window_->getSize().x, window_->getSize().y));
    window_->setView(*cam_);

    windSprite_.setOrigin(DataResources::windowTexture.getSize().x / 2, DataResources::windowTexture.getSize().y / 2);
    windSprite_.setTexture(DataResources::windowTexture);
    window_->setPosition((const sf::Vector2<int> &) cam_->getCenter());

    enterHeight_ = false;
    enterWidth_ = false;
    enterNum_ = false;
    playerFirstMove_ = true;

    generateMenu();
}

void MainMenu::drawMenu() {

    window_->draw(windSprite_);

    window_->draw(infoText_);

    window_->draw(inputWidth_);
    window_->draw(widthTxt_);

    window_->draw(inputHeight_);
    window_->draw(heightTxt_);

    window_->draw(inputNum_);
    window_->draw(numTxt_);

    window_->draw(testModeButton_);
    window_->draw(startGameButton_);
    window_->draw(testModeTxt_);
    window_->draw(startGameTxt_);

    window_->draw(additionalInfo_);

    window_->draw(firstMoveButton_);
    window_->draw(firstMoveText_);

}

void MainMenu::interact(float x, float y) {
    endEnteringText();

    if (inputWidth_.getGlobalBounds().contains(x, y)) {
        if (widthTxt_.getString() == ZERO_TEXT) {
            widthTxt_.setString("");
        }
        enterWidth_ = true;
    }

    if (inputHeight_.getGlobalBounds().contains(x, y)) {
        if (heightTxt_.getString() == ZERO_TEXT) {
            heightTxt_.setString("");
        }
        enterHeight_ = true;
    }

    if (inputNum_.getGlobalBounds().contains(x, y)) {
        if (numTxt_.getString() == ZERO_TEXT) {
            numTxt_.setString("");
        }
        enterNum_ = true;
    }

    if (startGameButton_.getGlobalBounds().contains(x, y)) {
        try {
            width_ = stoi((std::string) widthTxt_.getString());
            height_ = stoi((std::string) heightTxt_.getString());
            numberToWin_ = stoi((std::string) numTxt_.getString());
        } catch (std::invalid_argument &e) {
            return;
        }
        startGame();
    }
    if (testModeButton_.getGlobalBounds().contains(x, y)) {
        testMode_ = !testMode_;
        if (testMode_) {
            testModeTxt_.setString(TEST_MODE_ENABLED);
        } else {
            testModeTxt_.setString(TEST_MODE_DISABLED);
        }
    }

    if (firstMoveButton_.getGlobalBounds().contains(x, y)) {
        playerFirstMove_ = !playerFirstMove_;
        if (playerFirstMove_) {
            firstMoveText_.setString(CROSS_MOVE_TEXT);
        } else {
            firstMoveText_.setString(ZERO_MOVE_TEXT);
        }
    }
}

void MainMenu::enterText(char s) {

    if (s == '\b') {
        string g;
        if (enterWidth_) {
            g = widthTxt_.getString();
        } else if (enterHeight_) {
            g = heightTxt_.getString();
        } else if (enterNum_) {
            g = numTxt_.getString();
        } else {
            return;
        }
        if (!g.empty()) {
            g.pop_back();

        }
        if (enterWidth_) {
            widthTxt_.setString(g);
        } else if (enterHeight_) {
            heightTxt_.setString(g);
        } else if (enterNum_) {
            numTxt_.setString(g);
        }
        return;
    }

    int a = -1;

    try {
        s -= '0';
        a = (int(s));
        if (a < 0 || a > 9) {
            return;
        }
        s += '0';
    } catch (std::invalid_argument &e) {
        return;
    }

    if (widthTxt_.getString().getSize() < INPUT_FIELD_CHAR_LIMIT && enterWidth_) {
        widthTxt_.setString(widthTxt_.getString() + s);
    }

    if (heightTxt_.getString().getSize() < INPUT_FIELD_CHAR_LIMIT && enterHeight_) {
        heightTxt_.setString(heightTxt_.getString() + s);
    }

    if (numTxt_.getString().getSize() < INPUT_FIELD_CHAR_LIMIT && enterNum_) {
        numTxt_.setString(numTxt_.getString() + s);
    }
}

void MainMenu::startGame() {
    if (width_ > 0 && height_ > 0 && numberToWin_ <= std::max(width_, height_) && numberToWin_ > 0) {
        gm_ = new GameMaster(width_, height_, numberToWin_, playerFirstMove_, testMode_, window_);
    }
}

void MainMenu::resize(int x, int y) {
    cam_->setSize((float) x, (float) y);
    window_->setView(*cam_);
}

void MainMenu::deleteGame() {
    gm_ = nullptr;
    cam_ = new sf::View(sf::Vector2f(0, 0), sf::Vector2f(window_->getSize().x, window_->getSize().y));
    window_->setView(*cam_);
}

bool MainMenu::isEnteringText() {
    return enterWidth_ || enterHeight_ || enterNum_;
}

void MainMenu::endEnteringText() {
    enterHeight_ = false;
    enterWidth_ = false;
    enterNum_ = false;

}

GameMaster *MainMenu::getMaster() {
    return gm_;
}