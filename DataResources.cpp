//
// Created by ikmokhirio on 22.11.2019.
//
#include "DataResources.h"

sf::Texture DataResources::crossText;

sf::Texture DataResources::zeroText;

sf::Texture DataResources::cellText;

sf::Font DataResources::mainFont;

sf::Texture DataResources::windowTexture;

void DataResources::findError() {
    throw std::invalid_argument(FIND_ERROR_TEXT);
}

void DataResources::loadResources() {
    if (!DataResources::crossText.loadFromFile(CROSS_PATH)) {
        findError();
    }
    if (!DataResources::zeroText.loadFromFile(ZERO_PATH)) {
        findError();
    }
    if (!DataResources::cellText.loadFromFile(CELL_PATH)) {
        findError();
    }
    if (!DataResources::mainFont.loadFromFile(FONT_PATH)) {
        findError();
    }
    if (!DataResources::windowTexture.loadFromFile(WINDOW_PATH)) {
        findError();
    }
}
