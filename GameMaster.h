//
// Created by ikmokhirio on 19.11.2019.
//

#ifndef TIC_TAC_TOE_GAMEMASTER_H
#define TIC_TAC_TOE_GAMEMASTER_H

#include "Game.h"
#include "Minimax.h"
#include "DataResources.h"

class GameMaster {
private:

    int x_, y_, n_;

    bool playerFirstTurn_, playerCross_, playerTurn_, testMode_, endGame_;

    double zoomSpeed_, cameraSpeed_, zoom_;

    Game *gameInstance_;

    sf::Sprite crossSprite_;
    sf::Sprite zeroSprite_;
    sf::Sprite cellSprite_;

    sf::RenderWindow *gameWindow_;

    sf::View *camera_;
    sf::Vector2f oldCenterPos_;
    sf::Vector2f oldViewSize_;

    Minimax *robot_;

    sf::RectangleShape winBox_;
    sf::Text winText_;

    void setCrossWin();

    void setZeroWin();

public:

    bool isGameEnded();

    GameMaster(int x, int y, int n, bool firstMove, bool testMode, sf::RenderWindow *window);

    void drawGameField();

    void zoom(int in);

    void cameraMove(float x, float y, bool fixed);

    void makePlayerMove(int x, int y);

    sf::Vector2f getCellByClick(int x, int y);

    bool isPlayerCross();

    bool isTestMode();

    void makeMinimaxMove(bool cross = false);

    sf::View *getView();

    void gameRestart();

    bool isPlayerMove();

    void viewResize(int x, int y);
};


#endif //TIC_TAC_TOE_GAMEMASTER_H
