//
// Created by ikmokhirio on 08.11.2019.
//
#include "Field.h"

Field::Field(Field *f) {

    empty_ = f->empty_;
    fieldWidth_ = f->getWidth();
    fieldHeight_ = f->getHeight();

    field_ = new cell[fieldHeight_ * fieldWidth_];

    winCondition_ = f->winCondition_;

    crossScore_ = f->crossScore_;
    zeroScore_ = f->zeroScore_;
    oldCrossScore_ = f->oldCrossScore_;
    oldZeroScore_ = f->oldZeroScore_;

    for (int y = 0; y < fieldHeight_; y++) {
        for (int x = 0; x < fieldWidth_; x++) {
            field_[x + y * fieldWidth_] = f->get(x, y);
        }
    }
}

int Field::getMaxRow(int x, int y) {

    int count = 0;
    int max = 0;

    int t = x - (winCondition_ - 1);
    if (t < 0) {
        t = 0;
    }

    while (t < getWidth()) {
        if (get(t, y) == get(x, y)) {
            count++;
        } else {
            if (count > max) {
                max = count;
            }
            count = 0;
        }
        t++;
    }

    if (count > max) {
        max = count;
    }
    count = 0;


    t = y - (winCondition_ - 1);
    if (t < 0) {
        t = 0;
    }

    while (t < getHeight()) {
        if (get(x, t) == get(x, y)) {
            count++;
        } else {
            if (count > max) {
                max = count;
            }
            count = 0;
        }
        t++;
    }

    if (count > max) {
        max = count;
    }
    count = 0;

    int g = x;
    int k = y;

    while (g > 0 && k > 0) {
        g--;
        k--;
    }

    while (g < getWidth() && k < getHeight()) {

        if (get(g, k) == get(x, y)) {
            count++;
        } else {
            if (count > max) {
                max = count;
            }
            count = 0;
        }

        k++;
        g++;
    }

    if (count > max) {
        max = count;
    }
    count = 0;

    //Top right
    g = x;
    k = y;

    while (g > 0 && k < (getHeight() - 1)) {
        g--;
        k++;
    }

    while (g < getWidth() && k >= 0) {

        if (get(g, k) == get(x, y)) {
            count++;
        } else {
            if (count > max) {
                max = count;
            }
            count = 0;
        }

        k--;
        g++;
    }


    if (count > max) {
        max = count;
    }

    return max;
}


Field::Field(int f_w, int f_h, int n) {
    empty_ = true;
    fieldWidth_ = f_w;
    fieldHeight_ = f_h;

    field_ = new cell[fieldHeight_ * fieldWidth_];

    winCondition_ = n;

    crossScore_ = 0;
    zeroScore_ = 0;
    oldCrossScore_ = 0;
    oldZeroScore_ = 0;

    for (int y = 0; y < f_h; y++) {
        for (int x = 0; x < f_w; x++) {
            field_[x + y * f_w] = empty;
        }
    }
}

cell Field::get(int x, int y) {
    if (x > fieldWidth_ - 1 || x < 0 || y < 0 || y > fieldHeight_ - 1) {
        throw std::out_of_range("Out of range");
    }

    return field_[x + y * fieldWidth_];
}


void Field::set(int x, int y, cell type) {

    if (field_[x + y * fieldWidth_] != empty && type == empty) {
        if (field_[x + y * fieldWidth_ == cross]) {
            crossScore_ = oldCrossScore_;
        } else {
            zeroScore_ = oldZeroScore_;
        }
    }

    if (x > fieldWidth_ - 1 || x < 0 || y < 0 || y > fieldHeight_ - 1) {
        throw std::out_of_range("Out of range");
    }

    field_[x + y * fieldWidth_] = type;

    if (type == cross) {
        oldCrossScore_ = crossScore_;
        crossScore_ = getMaxRow(x, y);
    } else if (type == zero) {
        oldZeroScore_ = zeroScore_;
        zeroScore_ = getMaxRow(x, y);
    }


    empty_ = false;
}


bool Field::isEmpty() {
    return empty_;
}

void Field::setEmpty(bool empty) {
    empty_ = empty;
}

int Field::getScore(bool isCross) {
    if (isCross) return crossScore_;
    return -zeroScore_;
}

int Field::getHeight() {
    return fieldHeight_;
}

int Field::getWidth() {
    return fieldWidth_;
}
