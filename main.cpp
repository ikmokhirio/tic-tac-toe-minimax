#include <iostream>
#include "GameMaster.h"
#include "MainMenu.h"
#include "omp.h"


using namespace std;

int main() {

    //Init resources
    GameMaster *g = nullptr;
    bool fixedPos = false;
    bool deletingPhase = false;
    sf::Vector2f fixedMousePosition(0, 0);

    try {
        DataResources::loadResources();
    } catch (invalid_argument &e) {
        cout << e.what() << endl;
        return 0;
    }

    gameState state = menu;
    sf::RenderWindow window(sf::VideoMode(SCREEN_WIDTH, SCREEN_HEIGHT), GAME_NAME);
    MainMenu mainMenu(&window);
    //End init


#pragma omp parallel num_threads(2) default(none) shared(std::cout, window, state, g, mainMenu, fixedPos, fixedMousePosition, deletingPhase)
    {
        if (omp_get_thread_num() == MAIN_THREAD) {
            while (window.isOpen()) {
                sf::Event event{};

                while (window.pollEvent(event)) {

                    if (event.type == sf::Event::Closed ||
                        ((event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape) &&
                         state == menu)) {
                        window.close(); //Closing window on ESCAPE button in main menu
                    }

                    switch (state) {
                        case game: { //Control within game state
                            if (event.type == sf::Event::MouseWheelMoved) {
                                g->zoom(event.mouseWheel.delta);
                            } else if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
                                sf::Vector2i pixelPos = sf::Mouse::getPosition(window);
                                sf::Vector2f m = window.mapPixelToCoords(pixelPos);
                                g->makePlayerMove(m.x, m.y);
                            } else if (sf::Mouse::isButtonPressed(sf::Mouse::Right)) {
                                if (event.mouseMove.x != 0) {
                                    if (!fixedPos) {
                                        fixedMousePosition.x = sf::Mouse::getPosition(window).x;
                                        fixedMousePosition.y = sf::Mouse::getPosition(window).y;
                                        g->cameraMove(0, 0, fixedPos);
                                        fixedPos = true;
                                    } else {
                                        g->cameraMove(sf::Mouse::getPosition(window).x - fixedMousePosition.x,
                                                      sf::Mouse::getPosition(window).y - fixedMousePosition.y,
                                                      fixedPos);//Drag move
                                    }
                                }
                            } else if (event.type == sf::Event::KeyPressed) {
                                if (event.key.code == sf::Keyboard::Escape) {
                                    deletingPhase = true;
                                    delete g;
                                    g = nullptr;
                                    mainMenu.deleteGame();
                                    state = menu;
                                    deletingPhase = false;
                                } else if (event.key.code == sf::Keyboard::R) {
                                    g->gameRestart();
                                } else if (event.key.code == sf::Keyboard::H) {
                                    if (g->isPlayerMove()) {
                                        g->makeMinimaxMove(g->isPlayerCross());
                                    }
                                } else if (event.key.code == sf::Keyboard::Z) {
                                    g->makeMinimaxMove();
                                } else if (event.key.code == sf::Keyboard::C) {
                                    g->makeMinimaxMove(true);
                                }
                            } else if (event.type == sf::Event::Resized) {
                                g->viewResize(window.getSize().x, window.getSize().y);
                            } else {
                                fixedPos = false;
                            }
                            break;

                            case menu: { //Control within menu state

                                if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
                                    sf::Vector2i pixelPos = sf::Mouse::getPosition(window);
                                    sf::Vector2f m = window.mapPixelToCoords(pixelPos);
                                    mainMenu.interact(m.x, m.y);
                                    g = mainMenu.getMaster();
                                    if (g != nullptr) {
                                        state = game;
                                    }
                                } else if (mainMenu.isEnteringText() && event.type == sf::Event::TextEntered) {
                                    if (event.text.unicode == '\r' || event.text.unicode == '\n') {
                                        mainMenu.endEnteringText();
                                    }
                                    mainMenu.enterText(event.text.unicode);
                                } else if (event.type == sf::Event::Resized) {
                                    mainMenu.resize(window.getSize().x, window.getSize().y);
                                }
                                break;
                            }
                        }
                    }

                    //Drawing segment
                    window.clear(sf::Color::White);

                    switch (state) {
                        case game: {
                            g->drawGameField();
                            break;
                        }
                        case menu: {
                            mainMenu.drawMenu();
                            window.display();
                            break;
                        }
                    }


                }
            }
        } else if (omp_get_thread_num() == CALCULATION_THREAD) { //Move calculation
            while (window.isOpen()) {
                if (!deletingPhase && g != nullptr && !g->isGameEnded() && !g->isPlayerMove() && !g->isTestMode()) {
                    g->makeMinimaxMove(!g->isPlayerCross());
                }
            }
        }
    }

    return 0;
}