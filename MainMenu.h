//
// Created by ikmokhirio on 20.11.2019.
//

#ifndef TIC_TAC_TOE_MAINMENU_H
#define TIC_TAC_TOE_MAINMENU_H

#include "GameMaster.h"
#include "DataResources.h"

using namespace std;

class MainMenu {
private:

    bool testMode_;

    int width_, height_, numberToWin_;

    sf::Sprite windSprite_;

    sf::RenderWindow *window_;
    sf::View *cam_;

    GameMaster *gm_;

    bool enterWidth_, enterHeight_, enterNum_,playerFirstMove_;

    sf::Text infoText_;

    sf::RectangleShape startGameButton_, testModeButton_,firstMoveButton_;
    sf::RectangleShape inputWidth_, inputHeight_, inputNum_;
    sf::Text widthTxt_, heightTxt_, numTxt_, startGameTxt_, testModeTxt_,firstMoveText_;

    sf::Text additionalInfo_;


    void generateMenu();

    sf::RectangleShape
    generateShape(sf::Vector2f size, sf::Color mainColor, sf::Color outlineColor, float outlineThickness,
                  sf::Vector2f position);

    sf::Text generateText(sf::Vector2f position,string text,const sf::Font& font,sf::Color color, int characterSize);


public:



    MainMenu(sf::RenderWindow *window);

    void drawMenu();

    void startGame();

    GameMaster *getMaster();

    void deleteGame();

    bool isEnteringText();

    void endEnteringText();

    void enterText(char s);

    void resize(int x, int y);

    void interact(float x, float y);

};


#endif //TIC_TAC_TOE_MAINMENU_H
