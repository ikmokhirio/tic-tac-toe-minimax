//
// Created by ikmokhirio on 13.11.2019.
//

#include "Minimax.h"

Minimax::Minimax(Game *f) {
    oldGame_ = f;
    oldField_ = oldGame_->getField();
    fieldHeight_ = f->getField()->getHeight();
    fieldWidth_ = f->getField()->getWidth();
}


point Minimax::findBestMove(bool isCross, int depth) {

    game_ = new Game(oldGame_);
    field_ = game_->getField();

    point bestMove(0, 0, 0);

    int tmp = 0;

    if (isCross) {
        bestMove.cost = MINUS_INFINITY;
    } else {
        bestMove.cost = PLUS_INFINITY;
    }

    if (field_->isEmpty()) {
        bestMove.x = (fieldWidth_ - 1) / 2;
        bestMove.y = (fieldHeight_ - 1) / 2;
        bestMove.cost = 0;
        return bestMove;
    }

    for (int y = 0; y < fieldHeight_; y++) {
        for (int x = 0; x < fieldWidth_; x++) {

            if (field_->get(x, y) == empty && isAcross(x, y)) {
                if (isCross) {
                    field_->set(x, y, cross);

                    if (field_->getMaxRow(x, y) >= game_->getNumberToWin()) {
                        field_->set(x, y, empty);
                        bestMove.x = x;
                        bestMove.y = y;
                        bestMove.cost = game_->getWinScore();
                        return bestMove;
                    }
                } else {
                    field_->set(x, y, zero);

                    if (field_->getMaxRow(x, y) >= game_->getNumberToWin()) {
                        field_->set(x, y, empty);
                        bestMove.x = x;
                        bestMove.y = y;
                        bestMove.cost = -game_->getWinScore();
                        return bestMove;
                    }

                }

                tmp = minimax(!isCross, depth, MINUS_INFINITY, PLUS_INFINITY);

                if ((tmp > bestMove.cost && isCross) || (tmp < bestMove.cost && !isCross)) {
                    bestMove.cost = tmp;
                    bestMove.x = x;
                    bestMove.y = y;
                }

                field_->set(x, y, empty);

            }

        }
    }
    return bestMove;
}

int Minimax::minimax(bool maximize, int depth, int alpha, int beta) {

    int tmp = 0;

    if (game_->gameOver()) return 0;
    if (depth == 0) return game_->getPositionScore(maximize);

    if (maximize) {
        int max = MINUS_INFINITY;

        for (int x = 0; x < fieldWidth_; x++) {
            for (int y = 0; y < fieldHeight_; y++) {

                if (field_->get(x, y) == empty && isAcross(x, y)) {
                    field_->set(x, y, cross);

                    if ((field_->getMaxRow(x, y) >= game_->getNumberToWin())) {

                        field_->set(x, y, empty);
                        game_->reset();

                        return game_->getWinScore();
                    }

                    tmp = minimax(!maximize, depth - 1, alpha, beta);

                    alpha = std::max(alpha, tmp);

                    if (tmp > max) {
                        max = tmp;
                    }

                    field_->set(x, y, empty);

                    if (beta <= alpha) {
                        return max;
                    }
                }
            }
        }
        return max;
    } else {
        int min = PLUS_INFINITY;

        for (int x = 0; x < fieldWidth_; x++) {
            for (int y = 0; y < fieldHeight_; y++) {

                if (field_->get(x, y) == empty && isAcross(x, y)) {

                    field_->set(x, y, zero);

                    if ((field_->getMaxRow(x, y) >= game_->getNumberToWin())) {
                        field_->set(x, y, empty);
                        game_->reset();
                        return -game_->getWinScore();
                    }

                    tmp = minimax(!maximize, depth - 1, alpha, beta);

                    beta = std::min(beta, tmp);

                    if (tmp < min) {
                        min = tmp;
                    }

                    field_->set(x, y, empty);


                    if (beta <= alpha) {
                        return min;
                    }

                }
            }
        }
        return min;
    }
}

bool Minimax::isAcross(int x, int y) { //Check non empty neighbours

    if ((x > 0 && y > 0) && (field_->get(x - 1, y - 1) != empty)) {
        return true;
    }
    if ((x < fieldWidth_ - 1 && y < fieldHeight_ - 1) && (field_->get(x + 1, y + 1) != empty)) {
        return true;
    }
    if ((x < fieldWidth_ - 1) && (field_->get(x + 1, y) != empty)) {
        return true;
    }
    if ((x > 0) && (field_->get(x - 1, y) != empty)) {
        return true;
    }
    if ((y < fieldHeight_ - 1) && (field_->get(x, y + 1) != empty)) {
        return true;
    }
    if ((y > 0) && (field_->get(x, y - 1) != empty)) {
        return true;
    }
    if ((x > 0 && y < fieldHeight_ - 1) && (field_->get(x - 1, y + 1) != empty)) {
        return true;
    }
    if ((x < fieldWidth_ - 1 && y > 0) && (field_->get(x + 1, y - 1) != empty)) {
        return true;
    }

    return false;
}