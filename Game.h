//
// Created by ikmokhirio on 08.11.2019.
//

#ifndef TIC_TAC_TOE_GAME_H
#define TIC_TAC_TOE_GAME_H

#include "Field.h"
#include "Constants.h"

class Game {
private:

    bool crossTurn_,zeroWin_,crossWin_;

    Field *gameField_;

    int winCondition_ = 3; //Amount of chars in a row

public:

    int getNumberToWin();

    int getWinScore();

    void reset();

    bool gameOver();

    Game(int x, int y, int n);

    Game(Game *oldGame);

    bool move(int x, int y, cell cell); //True - success False - failure

    bool getCrossWin();

    bool getZeroWin();

    Field *getField();

    //int getMaxRow(int x, int y);

    int getPositionScore(bool isCross);

};


#endif //TIC_TAC_TOE_GAME_H
