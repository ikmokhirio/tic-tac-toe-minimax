//
// Created by ikmokhirio on 08.11.2019.
//

#include "Game.h"

Game::Game(int x, int y, int n) {
    gameField_ = new Field(x, y, n);

    winCondition_ = n;

    crossTurn_ = true;
    crossWin_ = false;
    zeroWin_ = false;
}

Game::Game(Game *oldGame) {
    gameField_ = new Field(oldGame->getField());

    winCondition_ = oldGame->winCondition_;

    crossTurn_ = oldGame->crossTurn_;
    crossWin_ = oldGame->crossWin_;
    zeroWin_ = oldGame->zeroWin_;
}

int Game::getNumberToWin() {
    return winCondition_;
}

int Game::getWinScore() {
    return WIN_SCORE;
}

bool Game::gameOver() {
    int f_w = gameField_->getWidth();
    int f_h = gameField_->getHeight();
    for (int i = 0; i < f_w; i++) {
        for (int j = 0; j < f_h; j++) {
            if (gameField_->get(i, j) == empty) {
                return false;
            }
        }
    }

    return true;
}

bool Game::move(int x, int y, cell c) {

    if ((crossTurn_ && c == cross) || (!crossTurn_ && c == zero)) {
        if (gameField_->get(x, y) == empty) {
            gameField_->set(x, y, c);
            crossTurn_ = !crossTurn_;

            if (gameField_->getMaxRow(x, y) >= winCondition_) {
                if (c == zero) {
                    zeroWin_ = true;
                }
                if (c == cross) {
                    crossWin_ = true;
                }
            }

            return true;
        }
    }

    return false;

}

Field *Game::getField() {
    return gameField_;
}

bool Game::getCrossWin() {
    return crossWin_;
}

bool Game::getZeroWin() {
    return zeroWin_;
}

void Game::reset() {
    crossWin_ = false;
    zeroWin_ = false;
}

int Game::getPositionScore(bool isCross) {

    if (isCross) {
        if (gameField_->getScore(!isCross) <= -winCondition_) {
            return -WIN_SCORE;
        } else {
            return gameField_->getScore(isCross);
        }
    } else {
        if (gameField_->getScore(!isCross) >= winCondition_) {
            return WIN_SCORE;
        } else {
            return gameField_->getScore(isCross);
        }
    }
}
