//
// Created by ikmokhirio on 22.11.2019.
//

#ifndef TIC_TAC_TOE_CONSTANTS_H
#define TIC_TAC_TOE_CONSTANTS_H

#include <iostream>

using namespace std;

const string GAME_NAME = "Tic-tac-toe";
const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;
const string ADDITIONAL_INFO = "ESC - menu\n"
                               "R - gameRestart game_\n"
                               "H - ask for help\n"
                               "IN TEST MODE:\n"
                               "Z - make move for zero\n"
                               "C - make move for cross\n";

const int INPUT_FIELD_CHAR_LIMIT = 6;

const string START_GAME_TEXT = "Start game";

const string TEST_MODE_DISABLED = "Test mode [X]";

const string TEST_MODE_ENABLED = "Test mode [V]";

const string ZERO_TEXT = "0";

const string INFO_TEXT = "WIDTH: \n\nHEIGHT: \n\nWIN: ";

const string CROSS_MOVE_TEXT = "CROSS";

const string ZERO_MOVE_TEXT = "ZERO";

const string CROSS_PATH = "data/cross.png";

const string ZERO_PATH = "data/zero.png";

const string CELL_PATH = "data/cell.png";

const string WINDOW_PATH = "data/window.png";

const string FONT_PATH = "data/arial.ttf";

const string FIND_ERROR_TEXT = "Can't find such file";

const float ZOOM_SPEED = 0.05f;

const float DEFAULT_ZOOM = 1.0f;

const int WIN_SCORE = 1000000;

const int MAIN_THREAD = 0;

const int CALCULATION_THREAD = 1;

const unsigned int RECURSION_DEPTH = 3;

const int PLUS_INFINITY = 9999999;

const int MINUS_INFINITY = -9999999;

const string CROSS_WIN = "CROSS WIN";

const string ZERO_WIN = "ZERO WIN";

const int THE_BIGGEST_TEXT = 42;

const int VERY_BIG_TEXT = 36;

const int BIG_TEXT = 32;

const int SMALL_TEXT = 24;

#endif //TIC_TAC_TOE_CONSTANTS_H
