//
// Created by ikmokhirio on 13.11.2019.
//

#ifndef TIC_TAC_TOE_MINIMAX_H
#define TIC_TAC_TOE_MINIMAX_H

#include "Game.h"
#include "Constants.h"

struct point {
public:
    int x, y;
    int cost;

    point(int x, int y, int cost) {
        this->x = x;
        this->y = y;
        this->cost = cost;
    }
};

//Maximaze = cross
//Minimize = zero

class Minimax {

private:

    int fieldHeight_;
    int fieldWidth_;

    Game *game_;
    Game *oldGame_;
    Field *field_;
    Field *oldField_;

    int minimax(bool maximize, int depth, int alpha, int beta);

    bool isAcross(int x, int y);

public:

    Minimax(Game *f);

    point findBestMove(bool maximize, int depth);

};


#endif //TIC_TAC_TOE_MINIMAX_H
