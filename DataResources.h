//
// Created by ikmokhirio on 22.11.2019.
//

#ifndef TIC_TAC_TOE_DATARESOURCES_H
#define TIC_TAC_TOE_DATARESOURCES_H

#include "SFML/Graphics.hpp"
#include "Constants.h"

enum gameState {
    menu,
    game
};

class DataResources {

private:
    static void findError();

public:
    static sf::Texture crossText, zeroText, cellText;

    static sf::Texture windowTexture;

    static sf::Font mainFont;

    static void loadResources();
};

#endif //TIC_TAC_TOE_DATARESOURCES_H
