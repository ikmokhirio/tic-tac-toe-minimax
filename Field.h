//
// Created by ikmokhirio on 08.11.2019.
//

#ifndef TIC_TAC_TOE_FIELD_H
#define TIC_TAC_TOE_FIELD_H

#include <stdexcept>
#include <iostream>


enum cell {
    zero = 0,
    cross = 1,
    empty = 2
};


class Field { //Tic tac toe gameField_
private:

    cell *field_;

    int fieldWidth_;
    int fieldHeight_;

    //test
    int crossScore_;
    int oldCrossScore_;
    int zeroScore_;
    int oldZeroScore_;
    int winCondition_;

    bool empty_;

public:

    bool isEmpty();

    void setEmpty(bool empty);

    Field(Field *f);

    Field(int f_w, int f_h, int n);

    void set(int x, int y, cell type);

    cell get(int x, int y);

    int getWidth();

    int getHeight();

    int getMaxRow(int x, int y);

    int getScore(bool isCross);

};

#endif //TIC_TAC_TOE_FIELD_H
