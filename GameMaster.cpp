//
// Created by ikmokhirio on 19.11.2019.
//

#include "GameMaster.h"


GameMaster::GameMaster(int x, int y, int n, bool firstMove, bool testMode, sf::RenderWindow *window) {

    this->testMode_ = testMode;

    cameraSpeed_ = 1;

    crossSprite_.setTexture(DataResources::crossText);
    zeroSprite_.setTexture(DataResources::zeroText);
    cellSprite_.setTexture(DataResources::cellText);


    gameInstance_ = new Game(x, y, n);
    x_ = x;
    y_ = y;
    n_ = n;

    playerFirstTurn_ = firstMove;

    camera_ = new sf::View(sf::Vector2f(gameInstance_->getField()->getWidth() * DataResources::cellText.getSize().x / 2,
                                        gameInstance_->getField()->getHeight() * DataResources::cellText.getSize().y /
                                        2),
                           sf::Vector2f(SCREEN_WIDTH, SCREEN_HEIGHT));


    this->gameWindow_ = window;

    window->setView(*camera_);

    zoomSpeed_ = ZOOM_SPEED;
    zoom_ = DEFAULT_ZOOM;

    oldViewSize_ = camera_->getSize();

    playerCross_ = firstMove;
    playerTurn_ = playerCross_;


    robot_ = new Minimax(gameInstance_);

    if (!playerTurn_) {
        makeMinimaxMove(!playerCross_);
    }

    endGame_ = false;

    winBox_.setSize(sf::Vector2f(SCREEN_WIDTH, SCREEN_HEIGHT));
    winBox_.setFillColor(sf::Color(255, 255, 255, 255));
    winBox_.setOutlineThickness(2.5);
    winBox_.setOutlineColor(sf::Color(0, 0, 0));
    winBox_.setOrigin(camera_->getSize().x / 2, camera_->getCenter().y / 2);
    winBox_.setPosition(camera_->getCenter());

    winText_.setString(ZERO_WIN);
    winText_.setPosition(camera_->getCenter());
    winText_.setFont(DataResources::mainFont);
    winText_.setFillColor(sf::Color::Black);
    winText_.setCharacterSize(VERY_BIG_TEXT);

}


void GameMaster::zoom(int in) {

    //camera_->zoom(1 - in * zoomSpeed_);
    zoom_ *= 1 - in * zoomSpeed_;
    camera_->setSize(oldViewSize_.x * zoom_, oldViewSize_.y * zoom_);

    winBox_.setSize(camera_->getSize());
    winBox_.setOrigin(camera_->getSize().x / 2, camera_->getCenter().y / 2);
    winBox_.setPosition(camera_->getCenter());

    winText_.setPosition(camera_->getCenter());
    winText_.setCharacterSize(VERY_BIG_TEXT * zoom_);

    gameWindow_->setView(*camera_);
}

void GameMaster::gameRestart() {

    playerCross_ = playerFirstTurn_;
    playerTurn_ = playerCross_;


    gameInstance_ = new Game(x_, y_, n_);
    camera_ = new sf::View(sf::Vector2f(gameInstance_->getField()->getWidth() * DataResources::cellText.getSize().x / 2,
                                        gameInstance_->getField()->getHeight() * DataResources::cellText.getSize().y /
                                        2),
                           sf::Vector2f(SCREEN_WIDTH, SCREEN_HEIGHT));

    robot_ = new Minimax(gameInstance_);

    zoomSpeed_ = ZOOM_SPEED;
    zoom_ = DEFAULT_ZOOM;

    gameInstance_->getField()->setEmpty(true);

    if (!playerTurn_) {
        makeMinimaxMove(!playerCross_);
    }

}

void GameMaster::drawGameField() {

    if (endGame_) {
        gameWindow_->draw(winBox_);
        gameWindow_->draw(winText_);
        gameWindow_->display();
        return;
    }

    int f_h = gameInstance_->getField()->getHeight();
    int f_w = gameInstance_->getField()->getWidth();


    for (int y = 0; y < f_h; y++) {
        for (int x = 0; x < f_w; x++) {
            if (gameInstance_->getField()->get(x, y) == zero) {
                zeroSprite_.setPosition(DataResources::zeroText.getSize().x * x,
                                        DataResources::zeroText.getSize().y * y);
                gameWindow_->draw(zeroSprite_);
            } else if (gameInstance_->getField()->get(x, y) == cross) {
                crossSprite_.setPosition(DataResources::crossText.getSize().x * x,
                                         DataResources::crossText.getSize().y * y);
                gameWindow_->draw(crossSprite_);
            } else {
                cellSprite_.setPosition(DataResources::cellText.getSize().x * x,
                                        DataResources::cellText.getSize().y * y);
                gameWindow_->draw(cellSprite_);
            }
        }
    }

    gameWindow_->display();
}

void GameMaster::viewResize(int x, int y) {
    oldViewSize_.x = x;
    oldViewSize_.y = y;
    zoom_ = DEFAULT_ZOOM;
    zoom(0);
}

bool GameMaster::isPlayerMove() {
    return playerTurn_;
}

void GameMaster::makePlayerMove(int m_x, int m_y) {

    if (!endGame_) {
        sf::Vector2f v = getCellByClick(m_x, m_y);

        if (v.x == -1 || v.y == -1) {
            return;
        }

        if (testMode_) {
            switch (gameInstance_->getField()->get(v.x, v.y)) {
                case (empty) : {
                    gameInstance_->getField()->set(v.x, v.y, cross);
                    break;
                }
                case (cross) : {
                    gameInstance_->getField()->set(v.x, v.y, zero);
                    break;
                }
                case (zero) : {
                    gameInstance_->getField()->set(v.x, v.y, empty);
                    break;
                }
            }
        } else if (playerTurn_) {
            if (v.x != -1 && v.y != -1 && (gameInstance_->getField()->get(v.x, v.y) == empty)) {
                if (playerCross_) {
                    gameInstance_->move(v.x, v.y, cross);
                    if (gameInstance_->getCrossWin()) {
                        setCrossWin();
                    }
                    if (gameInstance_->getZeroWin()) {
                        setZeroWin();
                    }
                } else {
                    gameInstance_->move(v.x, v.y, zero);
                    if (gameInstance_->getCrossWin()) {
                        setCrossWin();
                    }
                    if (gameInstance_->getZeroWin()) {
                        setZeroWin();
                    }
                }
                playerTurn_ = !playerTurn_;
            }
        }
    }
}

void GameMaster::setZeroWin() {
    endGame_ = true;
    winText_.setString(ZERO_WIN);
}

void GameMaster::setCrossWin() {
    endGame_ = true;
    winText_.setString(CROSS_WIN);
}

bool GameMaster::isGameEnded() {
    return endGame_;
}

bool GameMaster::isTestMode() {
    return testMode_;
}

void GameMaster::makeMinimaxMove(bool isCross) {
    point algoMovement(0, 0, 0);

    algoMovement = robot_->findBestMove(isCross, RECURSION_DEPTH);
    if (algoMovement.x == -1 || algoMovement.y == -1) {
        return;
    }

    if (isCross) {
        if (!testMode_) {
            gameInstance_->move(algoMovement.x, algoMovement.y, cross);

        } else {
            gameInstance_->getField()->set(algoMovement.x, algoMovement.y, cross);
        }
    } else {
        if (!testMode_) {
            gameInstance_->move(algoMovement.x, algoMovement.y, zero);
        } else {
            gameInstance_->getField()->set(algoMovement.x, algoMovement.y, zero);

        }
    }
    playerTurn_ = !playerTurn_;


    if (!testMode_ && gameInstance_->getCrossWin()) {
        setCrossWin();
    }
    if (!testMode_ && gameInstance_->getZeroWin()) {
        setZeroWin();
    }
}


bool GameMaster::isPlayerCross() {
    return playerCross_;
}

sf::Vector2f GameMaster::getCellByClick(int m_x, int m_y) {
    int f_h = gameInstance_->getField()->getHeight();
    int f_w = gameInstance_->getField()->getWidth();

    sf::Rect<int> *tmp;

    for (int y = 0; y < f_h; y++) {
        for (int x = 0; x < f_w; x++) {
            tmp = new sf::Rect<int>(DataResources::cellText.getSize().x * x, DataResources::cellText.getSize().y * y,
                                    DataResources::cellText.getSize().x,
                                    DataResources::cellText.getSize().y);

            if (tmp->contains(m_x, m_y)) {
                sf::Vector2f v(x, y);
                return v;
            }

        }
    }
    sf::Vector2f v(-1, -1);
    return v;
}

sf::View *GameMaster::getView() {
    return camera_;
}

void GameMaster::cameraMove(float x, float y, bool fixed) {

    if (!fixed) {
        oldCenterPos_ = camera_->getCenter();
        return;
    }

    camera_->setCenter(oldCenterPos_.x - (x * zoom_) * cameraSpeed_, oldCenterPos_.y - (y * zoom_) * cameraSpeed_);

    gameWindow_->setView(*camera_);
}